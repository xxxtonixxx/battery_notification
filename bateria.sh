#!/bin/bash
#
# -*- ENCODING: UTF-8 -*-
# Este programa es software libre. Puede redistribuirlo y/o
# modificarlo bajo los términos de la Licencia Pública General
# de GNU según es publicada por la Free Software Foundation,
# bien de la versión 2 de dicha Licencia o bien (según su
# elección) de cualquier versión posterior.
#
# Si usted hace alguna modificación en esta aplicación,
# deberá siempre mencionar al autor original de la misma.
#
# DesdeLinux.net CC-BY-SA 2015
# Autor: ELAV <elav@desdelinux.net> <http://www.systeminside.net>

# Obtenemos el estado de la batería, pero solamente el porciento:

BATERIA=`cat /sys/class/power_supply/BAT1/capacity`

if [ $BATERIA -ge 80 ]; then
        `notify-send --urgency=critical --expire-time=5000 --app-name=Bateria --icon=battery "Notificacion de Bateria" "Desconecta la corriente eléctrica por favor"`
fi        
if [ $BATERIA -le 40 ]; then
        `notify-send --urgency=critical --expire-time=5000 --app-name=Bateria --icon=battery "Notificacion de Bateria" "Conecta la corriente eléctrica por favor"`
fi
